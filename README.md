# Helm

Helm chart for LittleWorld

## Test

```
helm lint ./world/ && \
helm upgrade --install under ./world \
  --values ./world/values.yaml \
  --create-namespace --namespace underworld \
  --dry-run \
  --set ingress.hosts[0].host="under1.world.z64x.com" \
  --set ingress.tls[0].hosts[0]="under1.world.z64x.com"
```

## Install test

```
helm upgrade --install under ./world \
  --values ./world/values.yaml \
  --create-namespace --namespace underworld \
  --atomic --wait --timeout 4m \
  --set ingress.hosts[0].host="under.world.z64x.com" \
  --set ingress.tls[0].hosts[0]="under.world.z64x.com"
helm history --namespace underworld under
```

## Install prod

```
helm upgrade --install little ./world \
  --values ./world/values.yaml \
  --create-namespace --namespace littleworld \
  --atomic --wait --timeout 4m \
  --set ingress.hosts[0].host="little.world.z64x.com" \
  --set ingress.tls[0].hosts[0]="little.world.z64x.com"
helm history --namespace littleworld little
```
